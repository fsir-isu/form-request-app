<?php

/**
 * Зададим title страницы
 */
$title = 'Админка';

/**
 * Подключим шапку страницы
 */
include 'views/shared/header.php';

?>

  <h3>Админка</h3>

  <?php if (isset($message)) { ?>
  <p class="success"><?= $message ?></p>
  <?php } ?>

  <form method="POST" action="">

    <input type="hidden" name="_method" value="DELETE">

    <table border="1" cellspacing="0" cellpadding="5">
      <tr>
        <th></th>
        <th>ID</th>
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Телефон</th>
        <th>E-mail</th>
        <th>Тема</th>
        <th>Способ оплаты</th>
        <th>Рассылка</th>
      </tr>
      <?php foreach ($orders as $order) { ?>
      <tr>
        <td>
          <input type="checkbox" name="list_ids[]" value="<?= $order->id ?>">
        </td>
        <td>
          <a href="<?= '?id=' . $order->id ?>">
            <?= $order->id ?>
          </a>
        </td>
        <td><?= e($order->name) ?></td>
        <td><?= e($order->lastname) ?></td>
        <td><?= e($order->tel) ?></td>
        <td><?= e($order->email) ?></td>
        <td><?= $order->get_subject() ?></td>
        <td><?= $order->get_payment() ?></td>
        <td><?= $order->subscribe ? 'да' : '&mdash;' ?></td>
      </tr>
      <?php } ?>
    </table>

    <p>
      <button type="submit">
        Удалить выбранные
      </button>
    </p>

  </form>

<?php

/**
 * Подключим подвал страницы
 */
include 'views/shared/footer.php';

?>