<?php

/**
 * Зададим title страницы
 */
$title = 'Редактирование заявки';

/**
 * Подключим шапку страницы
 */
include 'views/shared/header.php';

?>

<h3><a href="?">Админка</a> &gt; Редактирование заявки</h3>

<?php

/**
 * Подключим форму редактирования
 */
include 'views/shared/form.php';

?>

<?php

/**
 * Подключим подвал страницы
 */
include 'views/shared/footer.php';

?>