
<?php if ($order->has_errors()) { ?>
<p class="error">Форма заполнена некорректно</p>
<?php } ?>

<?php if (isset($message)) { ?>
<p class="success"><?= $message ?></p>
<?php } ?>

<form action="" method="post" accept-charset="UTF-8">
  <table border="0">
    <tr>
      <td>Имя:</td>
      <td><input type="text" name="name" value="<?=e($order->name)?>"></td>
      <td class="error"><?=$order->get_error('name')?></td>
    </tr>
    <tr>
      <td>Фамилия:</td>
      <td><input type="text" name="lastname" value="<?=e($order->lastname)?>"></td>
      <td class="error"><?=$order->get_error('lastname')?></td>
    </tr>
    <tr>
      <td>Телефон:</td>
      <td><input type="text" name="tel" value="<?=e($order->tel)?>"></td>
      <td class="error"><?=$order->get_error('tel')?></td>
    </tr>
    <tr>
      <td>E-mail:</td>
      <td><input type="email" name="email" value="<?=e($order->email)?>"></td>
      <td class="error"><?=$order->get_error('email')?></td>
    </tr>
    <tr>
      <td>Подписаться на рассылку:</td>
      <td><input type="checkbox" name="subscribe" value="1"<?=$order->subscribe ? ' checked' : ''?>></td>
    </tr>
    <tr><td>Тематика:</td><td>
      <select name="subject">
        <?php foreach (Order::$subjects as $id => $subject) { ?>
        <option value="<?=$id?>"<?=$order->subject == $id ? ' selected' : ''?>><?=$subject?></option>
        <?php } ?>
      </select>
    </td></tr>
    <tr><td>Метод оплаты:</td><td>
      <select name="payment">
        <?php foreach (Order::$payments as $id => $payment) { ?>
        <option value="<?=$id?>"<?=$order->payment == $id ? ' selected' : ''?>><?=$payment?></option>
        <?php } ?>
      </select>
    </td></tr>
    <tr><td></td><td>
      <button type="submit">Отправить заявку</button>
    </td></tr>
  </table>
</form>
