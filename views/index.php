<?php

/**
 * Зададим title страницы
 */
$title = 'Заявка на участие в конференции';

/**
 * Подключим шапку страницы
 */
include 'views/shared/header.php';

?>

<h3>Заявка на участие в конференции</h3>

<?php

/**
 * Подключим форму заявки
 */
include 'views/shared/form.php';

?>

<?php

/**
 * Подключим подвал страницы
 */
include 'views/shared/footer.php';

?>