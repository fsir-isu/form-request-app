<?php

/**
 * Модель данных заявки
 */
class Order {

  /**
   * ID заявки
   * @var string
   */
  public $id;

  /**
   * Имя пользователя заявки
   * @var string
   */
  public $name;

  /**
   * Фамилия
   * @var string
   */
  public $lastname;

  /**
   * Телефон
   * @var string
   */
  public $tel;

  /**
   * E-mail
   * @var string
   */
  public $email;

  /**
   * Тема конференции
   * @var integer
   */
  public $subject = 3;

  /**
   * Метод оплаты
   * @var integer
   */
  public $payment;

  /**
   * Подписаться на рассылку новостей
   * @var boolean
   */
  public $subscribe = false;


  /**
   * Доступные темы конференции
   * @var Array
   */
  public static $subjects = [
    1 => 'Бизнес и коммуникации',
    2 => 'Технологии',
    3 => 'Реклама',
    4 => 'Маркетинг',
    5 => 'Проектирование',
  ];

  /**
   * Доступные методы оплаты
   * @var Array
   */
  public static $payments = [
    1 => 'WebMoney',
    2 => 'Яндекс.Деньги',
    3 => 'PayPal',
    4 => 'Кредитная карта',
    5 => 'Робокасса',
  ];


  /**
   * Конструктор объекта
   * 
   * @param Array $data Данные по объекту
   */
  public function __construct($data = null)
  {
    if ($data !== null && is_array($data))
    {
      $this->name      = trim(array_get($data, 'name'));
      $this->lastname  = trim(array_get($data, 'lastname'));
      $this->tel       = trim(array_get($data, 'tel'));
      $this->email     = trim(array_get($data, 'email'));
      $this->subject   = trim(array_get($data, 'subject'));
      $this->payment   = trim(array_get($data, 'payment'));
      $this->subscribe = trim(array_get($data, 'subscribe'));
    }
  }

  /**
   * Получаем директорию, куда сохраняем наши данные
   * 
   * @return string
   */
  public static function get_dir()
  {
    return 'models/data/';
  }

  /**
   * Переводим наш объект в строковое представление
   * 
   * @return string
   */
  public function to_string()
  {
    return $this->name . "\n" . 
           $this->lastname . "\n" .
           $this->tel . "\n" .
           $this->email . "\n" .
           $this->subject . "\n" .
           $this->payment . "\n" .
           $this->subscribe;
  }

  /**
   * Сохраняет объект
   * 
   * @return boolean Вернет true если сохранено успешно, false - при ошибках валидации
   */
  public function save()
  {
    /**
     * Проверим данные с помощью метода validate
     */
    if ( ! $this->validate())
    {
      /**
       * Валидация не прошла, прервем сохранение
       */
      return false;
    }
    else
    {
      /**
       * Получим директорию для данных
       */
      $dir = static::get_dir();

      if ( ! $this->id)
      {
        /**
         * Составим имя файла из текущей даты и времени, без расширения,
         * расширение .txt добавим позже
         */
        $filename = date('YmdHis');

        /**
         * Проверим, не существует ли случайно файл с таким именем
         * Вдруг в одну секунду было подано несколько заявок?
         * Если так, то добавим случайно число в конец
         */
        if (is_file($dir . $filename . '.txt'))
        {
          $filename .= '_' . rand(1, 20);
        }
      }
      else
      {
        $filename = $this->id;
      }

      /**
       * Сохраним данные в файл
       */
      file_put_contents($dir . $filename . '.txt', $this->to_string());

      return true;
    }
  }

  /**
   * Массив с ошибками валидации
   */
  protected $errors = [];

  /**
   * Валидирует (проверят) данные на корректность и сохраняет ошибки в массив,
   * потом эти ошибки мы сможем показать в форме
   * 
   * @return boolean Вернет true, если валидация данных прошла, false - в противном случае
   */
  public function validate()
  {
    if ($this->name === '')
    {
      $this->errors['name'] = 'Не введено имя';
    }

    if ($this->lastname === '')
    {
      $this->errors['lastname'] = 'Не введена фамилия';
    }

    if ($this->tel === '')
    {
      $this->errors['tel'] = 'Пустой телефон';
    }
    elseif (!preg_match('/^\+?[\d\(\) \-]+$/', $this->tel))
    {
      $this->errors['tel'] = 'Некорректный телефон';
    }
    elseif (strlen(preg_replace('/[^\d]/', '', $this->tel)) < 5)
    {
      $this->errors['tel'] = 'Некорректный телефон';
    }

    if ($this->email === '')
    {
      $this->errors['email'] = 'Пустой e-mail';
    }
    elseif (!preg_match('/^[\w\-\.]+@[\w\-\.]+$/', $this->email))
    {
      $this->errors['email'] = 'Некорректный e-mail';
    }

    return !$this->has_errors();
  }

  /**
   * Получить весь список ошибок валидации
   * 
   * @return Array
   */
  public function get_errors()
  {
    return $this->errors;
  }

  /**
   * Получить ошибку для конкретного свойства
   * 
   * @param  string $prop имя свойства
   * 
   * @return string
   */
  public function get_error($prop)
  {
    return isset($this->errors[$prop]) ? $this->errors[$prop] : '';
  }

  /**
   * Проверяет, если ли у объекта ошибки валидации
   * 
   * @return boolean
   */
  public function has_errors()
  {
    return count($this->errors) > 0;
  }

  /**
   * Возвращает текстовое представление выбранной темы
   * @return string
   */
  public function get_subject()
  {
    return array_get(Order::$subjects, $this->subject);
  }

  /**
   * Возвращает текстовое представление выбранного способа оплаты
   * @return string
   */
  public function get_payment()
  {
    return array_get(Order::$payments, $this->payment);
  }

  /**
   * Получение всех заявок
   * 
   * @return Array массив объектов класса Order
   */
  public static function all()
  {
    $dir = static::get_dir();

    /**
     * Создаем пустой массив для заявок
     * @var array
     */
    $orders = [];

    foreach (glob($dir . '*.txt') as $file)
    {
      $orders[] = Order::load_from_file($file);
    }

    return $orders;
  }

  /**
   * Загружает из файла ранее сохраненные данные
   * и создает объект на основе этих данных
   * 
   * @param  string $file
   * @return Order
   */
  public static function load_from_file($file)
  {
      $data = file_get_contents($file);

      $order = new Order;
      $order->import_from_string($data);
      $order->id = basename($file, '.txt');

      return $order;
  }

  /**
   * Импортирует объект из строкового представления
   * 
   * @param  string $string
   */
  public function import_from_string($string)
  {
    $data = explode("\n", $string);

    $this->name      = array_get($data, 0);
    $this->lastname  = array_get($data, 1);
    $this->tel       = array_get($data, 2);
    $this->email     = array_get($data, 3);
    $this->subject   = array_get($data, 4);
    $this->payment   = array_get($data, 5);
    $this->subscribe = array_get($data, 6);
  }

  /**
   * Поиск заявки по ее id
   * @param  integer|string $id id заявки
   * @return Order
   */
  public static function find($id)
  {
    $dir = Order::get_dir();

    /**
     * Создаем пустой объект
     * @var Order
     */
    $order = new Order;

    /**
     * Проверяем, есть ли файл заявки
     */
    if (is_file($dir . $id . '.txt'))
    {
      /**
       * Загружаем данные заявки из файла
       */
      $order = Order::load_from_file($dir . $id . '.txt');
    }

    return $order;
  }

  /**
   * Удаляет заявку
   * @return bool
   */
  public function delete()
  {
    $dir = Order::get_dir();

    if ($this->id and is_file($dir . $this->id . '.txt'))
    {
      unlink($dir . $this->id . '.txt');
      return true;
    }

    return false;
  }





}



