<?php

/**
 * Устанавливаем внутреннюю кодировку
 */
mb_internal_encoding('utf-8');

/**
 * Отчет об ошибках установим в E_ALL, что соответствуют отображению всех ошибок
 */
error_reporting(E_ALL);

/**
 * Инициализации сессии
 */
session_start();

/**
 * Подключаем функции и модели
 */
include 'functions.php';
include 'models/Order.php';

