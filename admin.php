<?php

/**
 * Подключаем всю автозагрузку (там необходимые функции, классы, настройки)
 */ 
include 'autoload.php';

/**
 * Если присутствует GET-параметр id, то нужно отобразить форму редактирования
 * Запрос может быть например такой: /admin.php?id=1
 */
if (isset($_GET['id']))
{
  $id = $_GET['id'];

  /**
   * Ищем заявку с заданным id
   * @var Order
   */
  $order = Order::find($id);

  /**
   * Проверим метод доступа
   */
  if ($_SERVER['REQUEST_METHOD'] === 'GET')
  {
    /**
     * Метод GET запрашивает отображение формы редактирования заявки
     */
    include 'views/admin/edit.php';
  }
  elseif ($_SERVER['REQUEST_METHOD'] === 'POST')
  {
    /**
     * Метод POST требует сохранения данных
     * 
     * Пройдемся по полученному массиву и обновим свойства модели Order
     */
    foreach ($_POST as $key => $value)
    {
      $order->$key = $value;
    }

    /**
     * Сохраняем измененную заявку
     */
    $order->save();

    /**
     * Отобразим форму заново
     *
     * (здесь также можно было бы сделать редирект)
     */
    include 'views/admin/edit.php';
  }
}
else
{
  if ($_SERVER['REQUEST_METHOD'] === 'GET')
  {

    /**
     * В случае, если для администратора есть сообщение, то передадим ее в представление
     * в виде переменной $message;
     */
    if (isset($_SESSION['message']))
    {
      $message = $_SESSION['message'];
      unset($_SESSION['message']);
    }

    /**
     * Через статичный метод all получим список всех заявок
     * @var array
     */
    $orders = Order::all();

    /**
     * Подключим представление, выводящее список заявок
     */
    include 'views/admin/list.php';
  }
  elseif ($_SERVER['REQUEST_METHOD'] === 'POST')
  {
    /**
     * Проверяем, нужно ли что-то удалить
     */
    if ($_POST['_method'] === 'DELETE')
    {
      if (isset($_POST['list_ids']))
      {
        /**
         * Эту переменную будем инкрементировать при каждом удалении
         * @var integer
         */
        $count = 0;

        /**
         * Пройдется по списку id заявок на удаление
         */
        foreach ($_POST['list_ids'] as $id)
        {
          /**
           * Находим нужную заявку по id
           * @var Order
           */
          $order = Order::find($id);

          /**
           * Удалим заявку
           */
          if ($order->delete())
          {
            /**
             * В случае успеха увеличим на единицу счетчик удаленных заявок
             */
            $count++;
          }
        }

        /**
         * Добавим сообщение для администратора
         */
        $_SESSION['message'] = 'Удалено заявок: ' . $count;
      }
    }

    /**
     * Редирект на изначальный адрес админки
     */
    header('Location: ' . $_SERVER['REQUEST_URI']);
  }
}


