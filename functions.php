<?php

/**
 * Вспомогательная функция для извлечения элемента из массива по заданному ключу,
 * дополнительно функция проверяет наличие ключа и возвращает значение по умолчанию,
 * если ключ не задан
 *  
 * @param  Array          $array    Исходный массив
 * @param  string|integer $key      Имя ключа или индекса
 * @param  mixed          $default  Значение по умолчанию
 * 
 * @return mixed
 */
function array_get($array, $key, $default = '')
{
  if (isset($array[$key]))
  {
    return $array[$key];
  }
  else
  {
    return $default;
  }
}

/**
 * Функция для экранирование HTML-спецсимволов
 *
 * Фактически это обертка для функции htmlspecialchars
 * 
 * @param  string $text
 * @return string
 */
function e($text)
{
  return htmlspecialchars($text);
}
